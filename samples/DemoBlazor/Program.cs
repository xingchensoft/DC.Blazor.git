using Ding.Dependency;
using Ding.Logs;
using Ding.Logs.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NewLife.Log;
using System;
using System.IO;
using System.Linq;

namespace DemoBlazor
{
    /// <summary>
    /// 应用程序
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 应用程序入口点
        /// </summary>
        /// <param name="args">入口点参数</param>
        public static void Main(string[] args)
        {
            try
            {
                XTrace.UseConsole();
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                ex.Log(Log.GetLog().Caption("应用程序启动失败"));
            }
        }

        /// <summary>
        /// 创建Web主机生成器
        /// </summary>
        /// <param name="args">入口点参数</param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new ServiceProviderFactory())
                .ConfigureServices(services => services.AddHttpContextAccessor())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .ConfigureAppConfiguration((context, config) =>
                    {
                        #region 查找 Settings目录
                        var env = context.HostingEnvironment;
                        var dir = Path.GetFullPath(env.ContentRootPath);
                        var settingsFolder = Path.Combine(dir, "Settings");

                        // 查找 Settings目录
                        if (!Directory.Exists(settingsFolder))
                        {
                            dir = Path.GetFullPath(env.ContentRootPath + "/..");
                        }

                        settingsFolder = Path.Combine(dir, "Settings");

                        if (!Directory.Exists(settingsFolder))
                        {
                            dir = Path.GetFullPath(env.ContentRootPath + "/bin");
                        }

                        settingsFolder = Path.Combine(dir, "Settings");
                        #endregion

                        var settings = Directory.GetFiles(settingsFolder, "*.json");
                        XTrace.WriteLine($"有多少数据：{settings.Count()}");
                        settings.ToList().ForEach(setting =>
                        {
                            config.AddJsonFile(setting, optional: false, reloadOnChange: true);
                        });
                    })
                    .UseStartup<Startup>();
                });
    }
}
