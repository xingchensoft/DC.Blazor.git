﻿using Ding.Datas.UnitOfWorks;

namespace DCCRM.Data
{
    /// <summary>
    /// 工作单元
    /// </summary>
    public interface IDCCRMUnitOfWork : IUnitOfWork
    {
    }
}
