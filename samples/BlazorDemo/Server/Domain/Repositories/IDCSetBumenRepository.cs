﻿using Ding.Domains.Repositories;
using DCCRM.Domain.Models;

namespace DCCRM.Domain.Repositories {
    /// <summary>
    /// 部门表仓储
    /// </summary>
    public interface IDCSetBumenRepository : IRepository<DCSetBumen,long> {
    }
}