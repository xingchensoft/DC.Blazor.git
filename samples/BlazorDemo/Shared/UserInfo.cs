﻿namespace BlazorDemo.Shared
{
    public class UserInfo
    {
        public bool IsAuthenticated { get; set; }

        public string Name { get; set; }
    }
}
