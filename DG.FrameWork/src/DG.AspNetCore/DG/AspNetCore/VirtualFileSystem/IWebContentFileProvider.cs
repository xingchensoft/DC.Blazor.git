﻿using Microsoft.Extensions.FileProviders;

namespace DG.AspNetCore.VirtualFileSystem
{
    public interface IWebContentFileProvider : IFileProvider
    {

    }
}
