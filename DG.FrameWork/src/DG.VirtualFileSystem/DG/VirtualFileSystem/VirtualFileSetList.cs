﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DG.VirtualFileSystem
{
    public class VirtualFileSetList : List<IVirtualFileSet>
    {
        public List<string> PhysicalPaths { get; }

        public VirtualFileSetList()
        {
            PhysicalPaths = new List<string>();
        }
    }
}
