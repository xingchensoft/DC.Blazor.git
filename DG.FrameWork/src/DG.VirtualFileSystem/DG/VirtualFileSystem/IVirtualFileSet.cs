﻿using Microsoft.Extensions.FileProviders;
using System.Collections.Generic;

namespace DG.VirtualFileSystem
{
    public interface IVirtualFileSet
    {
        void AddFiles(Dictionary<string, IFileInfo> files);
    }
}
