﻿namespace DG.VirtualFileSystem
{
    public class DGVirtualFileSystemOptions
    {
        public VirtualFileSetList FileSets { get; }

        public DGVirtualFileSystemOptions()
        {
            FileSets = new VirtualFileSetList();
        }
    }
}
