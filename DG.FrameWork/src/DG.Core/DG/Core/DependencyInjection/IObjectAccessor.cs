﻿using JetBrains.Annotations;

namespace DG.Core.DependencyInjection
{
    public interface IObjectAccessor<out T>
    {
        [CanBeNull]
        T Value { get; }
    }
}
