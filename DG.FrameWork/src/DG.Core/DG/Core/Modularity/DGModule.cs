﻿namespace DG.Core.Modularity
{
    public abstract class DGModule :
        IAbpModule
    {
        public virtual void ConfigureServices(ServiceConfigurationContext context)
        {

        }

    }
}
