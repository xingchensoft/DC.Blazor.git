﻿namespace DG.Core.Modularity
{
    public interface IAbpModule
    {
        void ConfigureServices(ServiceConfigurationContext context);
    }
}
