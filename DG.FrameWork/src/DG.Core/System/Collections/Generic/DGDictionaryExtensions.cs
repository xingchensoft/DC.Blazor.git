﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// 字典的扩展方法。
    /// </summary>
    public static class DGDictionaryExtensions
    {
        /// <summary>
        /// 使用给定的键从字典中获取一个值。 如果找不到，则返回默认值。
        /// </summary>
        /// <param name="dictionary">字典</param>
        /// <param name="key">要获取值的键</param>
        /// <typeparam name="TKey">键类型</typeparam>
        /// <typeparam name="TValue">值类型</typeparam>
        /// <returns>如果找到，则为找到的值，如果找不到，则为默认值。</returns>
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.TryGetValue(key, out var obj) ? obj : default;
        }
    }
}
