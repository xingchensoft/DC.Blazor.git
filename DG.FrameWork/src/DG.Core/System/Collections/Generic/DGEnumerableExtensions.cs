﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary> 
    /// <see cref="IEnumerable{T}"/> 的扩展方法。
    /// </summary>
    public static class DGEnumerableExtensions
    {
        /// <summary>
        /// 连接<see cref="IEnumerable{T}"/>中类型为System.String的集合为字符串, 在每个成员之间使用指定的分隔符。
        /// This is a shortcut for string.Join(...)
        /// </summary>
        /// <param name="source">包含要连接的字符串的集合。</param>
        /// <param name="separator">用作分隔符的字符串。 仅当值具有多个元素时，分隔符才包含在返回的字符串中。</param>
        /// <returns>一个字符串，包含由分隔符字符串分隔的值的成员。 如果values没有成员，则该方法返回System.String.Empty。</returns>
        public static string JoinAsString(this IEnumerable<string> source, string separator)
        {
            return string.Join(separator, source);
        }

        /// <summary>
        /// 使用每个成员之间的指定分隔符来连接集合的成员。
        /// 这是string.Join(...)的快捷方式
        /// </summary>
        /// <param name="source">包含要连接的对象的集合。</param>
        /// <param name="separator">用作分隔符的字符串。 仅当值具有多个元素时，分隔符才包含在返回的字符串中。</param>
        /// <typeparam name="T">值成员的类型。</typeparam>
        /// <returns>一个字符串，包含由分隔符字符串分隔的值的成员。 如果values没有成员，则该方法返回System.String.Empty。</returns>
        public static string JoinAsString<T>(this IEnumerable<T> source, string separator)
        {
            return string.Join(separator, source);
        }
    }
}
