﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// 集合的扩展方法。
    /// </summary>
    public static class DGCollectionExtensions
    {
        /// <summary>
        /// 检查给定的集合对象是否为null或没有内容。
        /// </summary>
        public static bool IsNullOrEmpty<T>([CanBeNull] this ICollection<T> source)
        {
            return source == null || source.Count <= 0;
        }
    }
}
