﻿using DG;
using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// String类的扩展方法。
    /// </summary>
    public static class DGStringExtensions
    {
        /// <summary>
        /// 如果给定的字符串不以char结尾，则将其添加到给定字符串的末尾。
        /// </summary>
        public static string EnsureEndsWith(this string str, char c, StringComparison comparisonType = StringComparison.Ordinal)
        {
            Check.NotNull(str, nameof(str));

            if (str.EndsWith(c.ToString(), comparisonType))
            {
                return str;
            }

            return str + c;
        }

        /// <summary>
        /// 如果给定的字符串不是以char开头，则将其添加到给定字符串的开头。
        /// </summary>
        public static string EnsureStartsWith(this string str, char c, StringComparison comparisonType = StringComparison.Ordinal)
        {
            Check.NotNull(str, nameof(str));

            if (str.StartsWith(c.ToString(), comparisonType))
            {
                return str;
            }

            return c + str;
        }

        /// <summary>
        /// 指示此字符串为null还是System.String.Empty字符串。
        /// </summary>
        public static bool IsNullOrEmptyForDG(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// 指示此字符串是null，空还是仅由空格字符组成。
        /// </summary>
        public static bool IsNullOrWhiteSpaceDG(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        /// <summary>
        /// 使用string.Split方法按给定的分隔符分隔给定的字符串。
        /// </summary>
        public static string[] Split(this string str, string separator)
        {
            return str.Split(new[] { separator }, StringSplitOptions.None);
        }
    }
}
