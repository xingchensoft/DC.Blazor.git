﻿using NewLife.Xml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DG.Cube
{
    /// <summary>SSL模式</summary>
    public enum SslModes
    {
        /// <summary>关闭</summary>
        [Description("关闭")]
        Disable = 0,

        /// <summary>仅首页</summary>
        [Description("仅首页")]
        HomeOnly = 10,

        /// <summary>所有请求</summary>
        [Description("所有请求")]
        Full = 9999,
    }

    /// <summary>DG设置</summary>
    [DisplayName("DG设置")]
    [XmlConfigFile(@"Config\Cube.config", 15_000)]
    public class Setting : XmlConfig<Setting>
    {
        #region 属性
        /// <summary>是否启用调试。默认为不启用</summary>
        [Description("调试")]
        public Boolean Debug { get; set; }

        /// <summary>工作台页面。进入后台的第一个内容页</summary>
        [Description("工作台页面。进入后台的第一个内容页")]
        public String StartPage { get; set; }

        /// <summary>布局页。</summary>
        [Description("布局页。")]
        public String Layout { get; set; } = "~/Views/Shared/_Ace_Layout.cshtml";

        /// <summary>默认角色。默认普通用户3</summary>
        [Description("默认角色。默认普通用户3")]
        public Int32 DefaultRole { get; set; } = 3;

        /// <summary>启用压缩。主要用于Json输出压缩，默认false</summary>
        [Description("启用压缩。主要用于Json输出压缩，默认false")]
        public Boolean EnableCompress { get; set; }

        /// <summary>用户在线。记录用户在线状态</summary>
        [Description("用户在线。记录用户在线状态")]
        public Boolean WebOnline { get; set; } = true;

        /// <summary>用户行为。记录用户所有操作</summary>
        [Description("用户行为。记录用户所有操作")]
        public Boolean WebBehavior { get; set; }

        /// <summary>访问统计。统计页面访问量</summary>
        [Description("访问统计。统计页面访问量")]
        public Boolean WebStatistics { get; set; } = true;

        /// <summary>强制SSL。强制使用https访问</summary>
        [Description("强制SSL。强制使用https访问")]
        public SslModes SslMode { get; set; } = SslModes.Disable;

        #endregion

        #region 方法
        /// <summary>实例化</summary>
        public Setting() { }

        protected override void OnLoaded()
        {
            if (StartPage.IsNullOrEmpty()) StartPage =
                "/Admin/Index/Main";

            if (DefaultRole <= 0) DefaultRole = 3;

            base.OnLoaded();
        }

        #endregion
    }
}
