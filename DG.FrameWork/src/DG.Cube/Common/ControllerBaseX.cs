﻿using DG.Cube.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DG.Cube
{
    /// <summary>控制器基类</summary>
    public class ControllerBaseX : Controller
    {
        #region 兼容处理
        /// <summary>获取请求值</summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual String GetRequest(String key) => Request.GetRequestValue(key);

        /// <summary>获取Session值</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual T GetSession<T>(String key) where T : class => HttpContext.Session.Get<T>(key);

        /// <summary>设置Session值</summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        protected virtual void SetSession(String key, Object value) => HttpContext.Session.Set(key, value);
        #endregion

        #region Ajax处理
        /// <summary>是否Json请求</summary>
        protected virtual Boolean IsJsonRequest
        {
            get
            {
                if (Request.ContentType.EqualIgnoreCase("application/json")) return true;

                if (Request.Headers["Accept"].Any(e => e.Split(',').Any(a => a.Trim() == "application/json"))) return true;

                if (GetRequest("output").EqualIgnoreCase("json")) return true;
                if ((RouteData.Values["output"] + "").EqualIgnoreCase("json")) return true;

                return false;
            }
        }
        #endregion

        #region Json结果
        /// <summary>响应Json结果</summary>
        /// <param name="code">代码。0成功，其它为错误代码</param>
        /// <param name="message">消息，成功或失败时的文本消息</param>
        /// <param name="data">数据对象</param>
        /// <param name="extend">扩展数据</param>
        /// <returns></returns>
        [NonAction]
        public virtual ActionResult Json(Int32 code, String message, Object data = null, Object extend = null)
        {
            if (data is Exception ex)
            {
                if (code == 0) code = 500;
                if (message.IsNullOrEmpty()) message = ex.GetTrue()?.Message;
                data = null;
            }

            Object rs = new { code, message, data };
            if (extend != null)
            {
                var dic = rs.ToDictionary();
                dic.Merge(extend);
                rs = dic;
            }

            return new JsonResult(rs);
        }
        #endregion
    }
}
