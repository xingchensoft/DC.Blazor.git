﻿using DG.AspNetCore.VirtualFileSystem;
using DG.Cube.Extensions;
using DG.Cube.WebMiddleware;
using DG.VirtualFileSystem;
using DG.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.WebEncoders;
using NewLife.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace DG.Cube
{
    /// <summary>DG服务</summary>
    public static class CubeService
    {
        #region 配置DG
        /// <summary>添加DG</summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCube(this IServiceCollection services)
        {
            // 修正系统名，确保可运行
            var set = SysConfig.Current;

            if (set.IsNew || set.Name == "DG.Cube.Views" || set.DisplayName == "DG.Cube.Views")
            {
                set.Name = "DG.Cube";
                set.DisplayName = "DG管理平台";
                set.SaveAsync();
            }

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            // 配置Cookie策略
            services.Configure<CookiePolicyOptions>(options =>
            {
                // 此项为true需要用户授权才能记录cookie
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // 添加Session会话支持
            services.AddSession();

            services.AddMvc(opt =>
            {
                // 分页器绑定
                opt.ModelBinderProviders.Insert(0, new PagerModelBinderProvider());

                // 模型绑定
                opt.ModelBinderProviders.Insert(0, new EntityModelBinderProvider());

                // 过滤器
                opt.Filters.Add<GlobalExceptionFilter>();

                //opt.EnableEndpointRouting = false;

            })
            // 添加版本兼容性，显示声明当前应用版本为3.0
            .SetCompatibilityVersion(CompatibilityVersion.Latest);

            // 注册虚拟文件系统
            services.AddVFS();

            //注入当前嵌入到虚拟文件系统
            services.Configure<DGVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<Setting>("DG.Cube");
                // options.FileSets.Add(new EmbeddedFileSet(typeof(MyModule).Assembly), "YourRootNameSpace");
            });

            // 添加压缩
            services.AddResponseCompression();

            // 防止汉字被自动编码
            services.Configure<WebEncoderOptions>(options =>
            {
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All);
            });

            return services;
        }
        #endregion

        #region 使用DG
        /// <summary>使用DG</summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseCube(this IApplicationBuilder app, IWebHostEnvironment env = null)
        {
            // 配置静态Http上下文访问器
            app.UseStaticHttpContext();

            var set = Setting.Current;

            // 压缩配置
            if (set.EnableCompress) app.UseResponseCompression();

            // 注册中间件
            app.UseVirtualFiles();
            app.UseCookiePolicy();
            app.UseSession();

            if (set.WebOnline || set.WebBehavior || set.WebStatistics) app.UseMiddleware<UserBehaviorMiddleware>();

            if (set.SslMode > SslModes.Disable) app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseRouting();
            // 设置默认路由
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    "CubeAreas",
                    "{area}/{controller=Index}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    "Default",
                    "{controller=CubeHome}/{action=Index}/{id?}"
                    );
                endpoints.MapRazorPages();
            })
            .Build();

            // 使用Cube前添加自己的管道
            if (env != null)
            {
                if (!env.IsDevelopment())
                    app.UseDeveloperExceptionPage();
                else
                    app.UseExceptionHandler("/CubeHome/Error");
            }

            return app;
        }
        #endregion

        #region 配置文件导入
        /// <summary>
        /// 从Settings导入配置文件
        /// </summary>
        public static void SetConfig(IWebHostEnvironment env, IConfigurationBuilder config)
        {
            var dir = Path.GetFullPath(env.ContentRootPath);
            var settingsFolder = Path.Combine(dir, "Settings");

            // 查找 Settings目录
            if (!Directory.Exists(settingsFolder))
            {
                dir = Path.GetFullPath(env.ContentRootPath + "/..");
            }

            settingsFolder = Path.Combine(dir, "Settings");

            if (!Directory.Exists(settingsFolder))
            {
                dir = Path.GetFullPath(env.ContentRootPath + "/bin");
            }

            settingsFolder = Path.Combine(dir, "Settings");

            if (Directory.Exists(settingsFolder))
            {
                var settings = Directory.GetFiles(settingsFolder, "*.json");
                settings.ToList().ForEach(setting =>
                {
                    config.AddJsonFile(setting, optional: false, reloadOnChange: true);
                });
            }
        }
#endregion
    }
}
