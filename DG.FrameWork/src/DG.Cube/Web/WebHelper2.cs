﻿using Microsoft.AspNetCore.Http;
using NewLife.Serialization;
using System;

namespace DG.Cube
{
    /// <summary>Web助手</summary>
    public static class WebHelper2
    {
        #region 兼容处理
        /// <summary>获取Session值</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(this ISession session, String key) where T : class
        {
            if (!session.TryGetValue(key, out var buf)) return default;

            return buf.ToStr().ToJsonEntity<T>();
        }

        /// <summary>设置Session值</summary>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Set(this ISession session, String key, Object value) => session.Set(key, value?.ToJson().GetBytes());

        /// <summary>获取用户主机IP</summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static String GetUserHost(this HttpContext context)
        {
            var request = context.Request;

            var str = "";
            if (str.IsNullOrEmpty()) str = request.Headers["HTTP_X_FORWARDED_FOR"];
            if (str.IsNullOrEmpty()) str = request.Headers["X-Real-IP"];
            if (str.IsNullOrEmpty()) str = request.Headers["X-Forwarded-For"];
            if (str.IsNullOrEmpty()) str = request.Headers["REMOTE_ADDR"];

            if (str.IsNullOrEmpty())
            {
                var addr = context.Connection?.RemoteIpAddress;
                if (addr != null)
                {
                    if (addr.IsIPv4MappedToIPv6) addr = addr.MapToIPv4();
                    str = addr + "";
                }
            }

            return str;
        }

        #endregion
    }
}
