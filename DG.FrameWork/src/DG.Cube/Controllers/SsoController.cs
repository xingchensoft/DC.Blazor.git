﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Cube.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NewLife.Model;
using NewLife.Web;
using XCode.Membership;

/*
 * DG OAuth在禁用本地登录，且只设置一个第三方登录时，形成单点登录。
 * 
 * 验证流程：
 *      进入登录页~/Admin/User/Login
 *      if 允许本地登录
 *          输入密码登录 或 选择第三方登录
 *      else if 多个第三方登录
 *          选择第三方登录
 *      else
 *          直接跳转唯一的第三方登录
 *      登录完成
 *      if 有绑定用户
 *          登录完成，跳转来源页
 *      else
 *          进入绑定流程
 * 
 * 绑定流程：
 *      if 本地已登录
 *          第三方绑定到当前已登录本地用户
 *      else 允许本地登录
 *          显示登录框，输入密码登录后绑定（暂不支持）
 *          或 直接进入，自动注册本地用户
 *      else
 *          直接进入，自动注册本地用户
 */

namespace DG.Cube.Controllers
{
    /// <summary>单点登录控制器</summary>
    public class SsoController : ControllerBaseX
    {
        /// <summary>当前提供者</summary>
        public static SsoProvider Provider { get; set; }

        /// <summary>单点登录服务端</summary>
        public static OAuthServer OAuth { get; set; }

        static SsoController()
        {
            // 注册单点登录
            var oc = ObjectContainer.Current;
            oc.AutoRegister<SsoProvider, SsoProvider>();
            oc.AutoRegister<OAuthServer, OAuthServer2>();

            Provider = ObjectContainer.Current.ResolveInstance<SsoProvider>();
            OAuth = ObjectContainer.Current.ResolveInstance<OAuthServer>();

            //OAuthServer.Instance.Log = XTrace.Log;
            OAuth.Log = LogProvider.Provider.AsLog("OAuth");
        }


        /// <summary>首页</summary>
        /// <returns></returns>
        [AllowAnonymous]
        public virtual ActionResult Index() => Redirect("~/");

    }
}
