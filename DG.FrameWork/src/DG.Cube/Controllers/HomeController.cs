﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DG.Cube.Controllers
{
    /// <summary>主页面</summary>
    public class CubeHomeController : ControllerBaseX
    {
        /// <summary>主页面</summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Message = "主页面";

            return View();
        }

        /// <summary>错误</summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var context = HttpContext.Items["ExceptionContext"] as ExceptionContext;
            if (IsJsonRequest)
            {
                if (context?.Exception != null)
                    return Json(500, null, context.Exception, new { action = context.ActionDescriptor.DisplayName });
            }

            return View("Error", context);
        }
    }
}
