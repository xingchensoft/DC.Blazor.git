﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;

namespace DG.Cube.Extensions
{
    /// <summary>请求助手类</summary>
    public static class RequestHelper
    {
        /// <summary>
        /// 从请求中获取值
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static String GetRequestValue(this HttpRequest request, String key)
        {
            var value = new StringValues();

            if (request.HasFormContentType) value = request.Form[key];

            if (value.Count > 0) return value;

            value = request.Query[key];
            return value.Count > 0 ? value.ToString() : null;
        }
    }
}
