﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DCBlazor
{
    public static class Builder
    {
        /// <summary>
        /// 配置
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        public static void UseDG(this IServiceCollection services, IConfiguration configuration)
        {
            Configuration = configuration;
            Console.WriteLine("获取到的站点标题" + Configuration["SiteName"]);
        }

        public static void UseDG(this IApplicationBuilder applicationBuilder, IWebHostEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            ServiceLocator.Setup(httpContextAccessor);
        }
    }
}
