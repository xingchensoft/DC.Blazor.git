﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;

namespace DCBlazor
{
    /// <summary>
    /// 页面处理
    /// </summary>
    public class PageGenerator
    {
        public static ConcurrentDictionary<string, PageInfo> PageInfo = new ConcurrentDictionary<string, PageInfo>();

        /// <summary>
        /// 添加页面信息
        /// </summary>
        /// <param name="pageInfo"></param>
        public static void ProcessPage(List<PageInfo> pageInfo)
        {
            if (pageInfo == null || pageInfo.Count == 0) return;
            pageInfo.ForEach(x =>
            {
                if (PageInfo.ContainsKey(x.Url)) return;
                PageInfo.TryAdd(x.Url, x);
            });
        }

        public static string Create(string pageName)
        {
            if (string.IsNullOrWhiteSpace(pageName))
                throw new ArgumentException("值不能为null或空.", nameof(pageName));

            string pageTitle = "";
            var textInfo = CultureInfo.CurrentCulture.TextInfo;

            pageName = pageName.Replace('-', ' ');

            foreach(var row in PageInfo)
            {
                if (row.Value == null) continue;

                if (row.Key == pageName)
                {
                    if (pageName == "/" && string.IsNullOrWhiteSpace(row.Value.Title))
                    {
                        pageTitle = Builder.Configuration["SiteName"];
                    }
                    else
                    {
                        pageTitle = $"{textInfo.ToTitleCase(row.Value.Title)} | {Builder.Configuration["SiteName"]}";
                    }
                    break;
                }
            }

            return pageTitle;
        }
    }
}
