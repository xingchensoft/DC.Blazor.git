﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DCBlazor
{
    /// <summary>
    /// 修改页面标题
    /// </summary>
    public class Document : ComponentBase, IDisposable
    {
        /// <summary>
        /// JavaScript运行时代码
        /// </summary>
        [Inject]
        protected IJSRuntime JsRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                NavigationManager.LocationChanged -= LocationChanged;
        }

        protected override void OnInitialized()
        {
            NavigationManager.LocationChanged += LocationChanged;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender) return;

            await SetTitle(new Uri(NavigationManager.Uri));
        }

        private async Task SetTitle(Uri uri)
        {
            var pageName = uri.Segments.Last();
            await JsRuntime.InvokeVoidAsync("setDocumentTitle", PageGenerator.Create(pageName));
        }

        private async void LocationChanged(object sender, Microsoft.AspNetCore.Components.Routing.LocationChangedEventArgs e)
        {
            await SetTitle(new Uri(e.Location));
        }
    }
}
