﻿namespace DCBlazor
{
    /// <summary>
    /// 页面信息
    /// </summary>
    public class PageInfo
    {
        /// <summary>
        /// 不带域名的路由
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 页面标题
        /// </summary>
        public string Title { get; set; }
    }
}
