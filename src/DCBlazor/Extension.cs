﻿using DCBlazor.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace DCBlazor
{
    public static class Extension
    {
        public static IServiceCollection AddDCBlazorServices(this IServiceCollection services)
        {
            services.AddDCBlazorStorage();
            services.AddHttpClient();
            return services;
        }
    }
}
